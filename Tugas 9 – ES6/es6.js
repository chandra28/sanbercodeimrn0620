//SOAL NOMOR 1
console.log("\nJawaban Nomor 1")
console.log("-----------------")
const golden = () => {
    console.log("this is golden!!")
}

golden()

//SOAL NOMOR 2
console.log("\nJawaban Nomor 2")
console.log("-----------------")
const newFunction = (firstName, lastName) => {

    return {
        firstName,
        lastName,
        fullName: () => {
            console.log(`${firstName} ${lastName}`)
        }
    }

}

newFunction("William", "Imoh").fullName();

//SOAL NOMOR 3
console.log("\nJawaban Nomor 3")
console.log("-----------------")
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation, spell } = newObject;
console.log(firstName, lastName, destination, occupation)

//SOAL NOMOR 4
console.log("\nJawaban Nomor 4")
console.log("-----------------")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east];
console.log(combined)

//SOAL NOMOR 5
console.log("\nJawaban Nomor 5")
console.log("-----------------")
const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet , consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`;
console.log(before)