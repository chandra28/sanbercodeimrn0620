//SOAL NOMOR 1
console.log("\nJawaban Nomor 1")
console.log("RELEASE 0")
console.log("---------")

class Animal {
    constructor(name) {
        this.name = name;
        this.legs = 4;
        this.cold_blooded = false;
    }
}

var sheep = new Animal("shaun");
console.log(sheep.name);// "shaun"
console.log(sheep.legs);// 4
console.log(sheep.cold_blooded);// false

console.log("RELEASE 1")
console.log("---------")
class Ape extends Animal {
    constructor() {
        super().legs = 2;
    }
    yell() {
        console.log("Auooo")// "Auooo"
    }
}

class Frog extends Animal {
    constructor() {
        super()
    }
    jump() {
        console.log("hop hop")// "hop hop"
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell()

var kodok = new Frog("buduk")
kodok.jump()

//SOAL NOMOR 2
console.log("\nJawaban Nomor 2")
class Clock {
    constructor({ template }) {
        this.template = template;
        this.timer = 0;
    }

    render() {
        var date = new Date();
        var hours = date.getHours();
        if (hours < 10) {
            hours = '0' + hours;
        }
        var mins = date.getMinutes();
        if (mins < 10) {
            mins = '0' + mins;
        }
        var secs = date.getSeconds();
        if (secs < 10) {
            secs = '0' + secs;
        }
        var output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);
        console.log(output)
    }

    stop() {
        clearInterval(this.timer);
    }

    start() {
        this.timer = setInterval(this.render.bind(this), 1000);
    }
}

var clock = new Clock({ template: 'h:m:s' });
clock.start();  