console.log('No. 1 Looping While ');
console.log('LOOPING PERTAMA');
for (var deret = 2; deret < 22; deret += 2) {
    console.log(deret + '- I love coding');
}

console.log('LOOPING KEDUA');

for (var deret = 20; deret > 0; deret -= 2) {
    console.log(deret + '- I will become a mobile developer ');
}


console.log('No. 2 Looping menggunakan for');
console.log('OUTPUT')
var a = 1;
for (i = a; i <= 20; i++) {
    if ((i % 3) === 0 && (i % 2) === 1) {
        console.log(i + '- ilove coding')
    } else if ((i % 2) === 0) {
        console.log(i + '- berkualitas')
    } else {
        console.log(i + '- santai');
    }
}


console.log('No. 3 Membuat Persegi Panjang');
var s = '';
for (var i = 0; i < 4; i++) {
    for (var j = 0; j < 8; j++) {
        s += '#';
    }
    s += '\n';
}
console.log(s);

console.log('No. 4 Membuat Persegi Panjang');
var s = '';
for (var i = 0; i < 8; i++) {
    for (var j = 1; j <= i; j++) {
        s += '#';
    }
    s += '\n';
}
console.log(s);

console.log('No. 5 Membuat Papan Catur');
var s = '';
for (var i = 1; i <= 8; i++) {
    if ((i % 2) == 1) {
        s += ' ';
    }
    for (var k = 1; k <= 4; k++) {
        s += "# ";
    }
    s += '\n';
}
console.log(s)



